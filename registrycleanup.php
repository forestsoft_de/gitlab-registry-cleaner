<?php
include_once dirname(__FILE__) . '/vendor/autoload.php';

include_once "src/DeleteRequestAlreadyTakenException.php";
include_once "src/GitlabClient.php";
$deleteRegex = (!empty($_ENV['DELETE_REGEX'])) ? $_ENV['DELETE_REGEX'] : "";
try {
    if (empty($_ENV['CI_PROJECT_ID'])) {
        throw new \InvalidArgumentException("Please define env variable CI_PROJECT_ID");
    }
    if (empty($_ENV['API_TOKEN'])) {
        throw new \InvalidArgumentException("Please define env variable API_TOKEN with api read and write permissions");
    }

    $client = new GitlabClient(intval($_ENV['CI_PROJECT_ID']), $_ENV['API_TOKEN'], $deleteRegex);
    $repositories = $client->fetchRepositories();
    foreach ($repositories as $repo) {
        if (!empty($_ENV['REPO'])) {
            if ($repo['path'] !== $_ENV['REPO']) {
                echo "\nIgnore Repo: " . $repo['path'] . " for " . $_ENV['REPO'];
                continue;
            }
        }
        try {
            if (!empty($_ENV['CLEANUP_TIME'])) {
                echo "\nClean images for " .  $repo['path'] . " with cleanup time " . $_ENV['CLEANUP_TIME'];
                $result = $client->cleanRepositoryOlderThan($repo['id'], $_ENV['CLEANUP_TIME']);
            }
            if (!empty($_ENV['KEEP'])) {
                echo "\nClean images for " .  $repo['path'] . " and keep " . $_ENV['KEEP'] . " images";
                $result = $client->cleanRepositoryAndKeep($repo['id'], intval($_ENV['KEEP']));
            }
        } catch (DeleteRequestAlreadyTakenException $e) {
            $result['message'] = "Delete request already taken within last hour for " . $repo['path'];
        }
        echo "\n";
        if ($result['result'] == "202") {
            echo "Operation successfully";
        }
        print_r($result);
    }
} catch (\InvalidArgumentException $e) {
  echo $e->getMessage();
  exit (255);
} catch (\GuzzleHttp\Exception\ClientException $e) {
    echo "\nError:\n";
    echo "\nHTTP-Status:" . $e->getResponse()->getStatusCode(). "\n";
    echo $e->getResponse()->getBody()->getContents();
    exit(255);
}
exit(0);