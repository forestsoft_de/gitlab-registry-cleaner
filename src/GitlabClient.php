<?php declare(strict_types=1);


class GitlabClient
{
    CONST API_VERSION = "4";

    protected \GuzzleHttp\Client $_client;

    private string $baseDomain = 'https://gitlab.com/api/v' . self::API_VERSION;


    private string $endPoint = '';

    private string $apiToken = '';

    private int $projectId = 0;

    private string $deleteRegex = ".*";

    /**
     * GitlabClient constructor.
     * @param int $projectId
     * @param string $apiToken
     * @param string $deleteRegex
     */
    public function __construct(int $projectId, string $apiToken, string $deleteRegex = ".*")
    {
        $this->apiToken = $apiToken;

        $config = [
            'headers' =>
                [
                    'PRIVATE-TOKEN' => $this->apiToken,
                    'Authorization' => "Bearer " . $this->apiToken,
                ]
        ];
        $this->_client = new GuzzleHttp\Client($config);
        $this->projectId = $projectId;
        if (!empty($deleteRegex)) {
            $this->deleteRegex = $deleteRegex;
        }
    }

    public function fetchRepositories() : array
    {
        return $this->performRequest($this->apiEndPoint() . '/projects/' . $this->getProjectId() . '/registry/repositories');
    }

    private function getProjectId() : int
    {
        return $this->projectId;
    }

    private function apiEndPoint() : string
    {
        if (empty($this->endPoint)) {
            $this->endPoint = $this->baseDomain;
        }
        return $this->endPoint;
    }

    /**
     * erase all images older than specific time
     *
     * @param int $id
     * @param string $time i.e. 1h, 1d, 1month.
     *
     * @return array
     */
    public function cleanRepositoryOlderThan(int $id, string $time) : array
    {
        return $this->cleanTags($id, ['older_than' => $time]);
    }

    /**
     * Erase images and keep keecpCount items.
     *
     * @param int $id
     * @param int $keepCount
     * @return array
     */
    public function cleanRepositoryAndKeep(int $id, int $keepCount): array
    {
        return $this->cleanTags($id, ['keep_n' => $keepCount]);
    }

    /**
     * Erase all images except of latest
     *
     * @param int $id
     * @return array
     */
    public function cleanRepository(int $id) : array
    {
        return $this->cleanTags($id);
    }



    private function cleanTags(int $id, array $config) : array
    {
        $defaultConfig = [
            'name_regex_delete' => $this->deleteRegex
        ];

        $config = array_merge($defaultConfig, $config);

        $params['multipart'] = [];

        foreach ($config as $key => $value) {
            array_push($params['multipart'], ['name' => $key, 'contents' => $value]);
        }

        try {
            return $this->performRequest($this->apiEndPoint() . '/projects/' . $this->getProjectId() . '/registry/repositories/' . $id . "/tags", "DELETE", $params);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            if (stristr(strtolower($e->getMessage()), "this request has already been made")) {
                throw new \DeleteRequestAlreadyTakenException();
            }
        }
    }

    private function performRequest(string $url, string $method = "GET", ?array $params = []) : array
    {
        $result = $this->_client->request($method, $url, $params);
        $result = $result->getBody()->getContents();

        if (substr($result, 0, 1) == '{' || substr($result, 0, 1) == '[') {
            $data = \json_decode($result, true);
            return $data;
        } else {
            return ['result' => $result];
        }
    }
}