### Usage
To delete an specific repo specifiy environment variable REPO with the path to the repo i.e: projectname/repo
If repo is ommitted all repositories for the given project id will be cleaned except latest tag.

````bash
docker run --rm \
          -e API_TOKEN="${API_TOKEN}" \
          -e CI_PROJECT_ID=${CI_PROJECT_ID} \
          -e REPO=${CI_REPO} \
          -e DELETE_REGEX=".*" \
          -e KEEP="5" \
          registry.gitlab.com/forestsoft_de/gitlab-registry-cleaner
````
