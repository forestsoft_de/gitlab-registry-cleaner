FROM php:7.4-alpine3.13

RUN apk --no-cache add git \
   && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
   && mkdir -p /var/cache/composer \
   && chmod 777 /var/cache/composer \
   && mkdir -p /app/

WORKDIR /app/
COPY composer.json /app/composer.json
RUN composer update

COPY . /app/

CMD php /app/registrycleanup.php